package exception;

import javafx.scene.control.Button;

public class NoteNoFoundException extends RuntimeException {
    public NoteNoFoundException() {
        super();
    }

    public NoteNoFoundException(String message) {
        super(message);
    }

    public NoteNoFoundException(String message, Throwable cause) {
        super(message, cause);
        Button button = new Button();
    }
}
