package controller;


import exception.NoteNoFoundException;
import model.Note;
import repository.repositoryevent.NoteCreateEvent;
import repository.repositoryevent.NoteDeleteEvent;
import service.Observer.reciver.Receiver;
import service.api.NoteAPI;


import java.util.List;

public class NoteController {
private NoteAPI noteAPI;

    public NoteController(NoteAPI noteAPI) {
        this.noteAPI = noteAPI;
    }


    public void create (Note note) {
        noteAPI.create(note);
    }

    public void delete (Note note) {
        noteAPI.delete(note);
    }

    public void edit (Note note) throws NoteNoFoundException {
        noteAPI.edit(note);
    }

    public Note getNote(Long id) {
        return noteAPI.get(id);
    }

    public List<Note> getAll(){
        return  noteAPI.getAll();
    }


    void setOnCreateNoteReceiver(Receiver<NoteCreateEvent> noteCreateEventReceiver){
        noteAPI.setOnCreateNoteReceiver(noteCreateEventReceiver);
    }

    void removeCreateNoteReceiver(Receiver<NoteCreateEvent> noteCreateEventReceiver){
        noteAPI.removeCreateNoteReceiver(noteCreateEventReceiver);
    }

    void setOnDeleteNoteReceiver(Receiver<NoteDeleteEvent> noteCreateEventReceiver){
        noteAPI.setOnDeleteNoteReceiver(noteCreateEventReceiver);
    }

    void removeDeleteNoteReceiver(Receiver<NoteDeleteEvent> noteCreateEventReceiver){
        noteAPI.setOnDeleteNoteReceiver(noteCreateEventReceiver);
    }

}
