package service.generator;

import service.maxIdGetter.MaxIdGetter;

import java.util.concurrent.atomic.AtomicLong;

public class LocalIdGeneratorImpl implements IdGenerator  {
    private AtomicLong generId ;

    public LocalIdGeneratorImpl( MaxIdGetter maxIdGetter ) {
        this.generId = new AtomicLong( maxIdGetter.getMaxId() );
    }

    @Override
    public long generate() {
        return generId.incrementAndGet();
    }
}
