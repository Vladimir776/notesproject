package service.generator;

public interface IdGenerator {
    long generate();
}
