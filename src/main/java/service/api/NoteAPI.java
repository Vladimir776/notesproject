package service.api;

import model.Note;
import repository.repositoryevent.NoteCreateEvent;
import repository.repositoryevent.NoteDeleteEvent;
import service.Observer.reciver.Receiver;

import java.util.List;


public interface NoteAPI {

    Note create(Note note);

    Note get(long id);

    void delete(Note note);

    List<Note> getAll();

    Note edit (Note note);

    void setOnCreateNoteReceiver(Receiver<NoteCreateEvent> noteCreateEventReceiver);

    void removeCreateNoteReceiver(Receiver<NoteCreateEvent> noteCreateEventReceiver);

    void setOnDeleteNoteReceiver(Receiver<NoteDeleteEvent> noteDeleteEventReceiver);

    void removeDeleteNoteReceiver(Receiver<NoteDeleteEvent> noteDeleteEventReceiver);
}
