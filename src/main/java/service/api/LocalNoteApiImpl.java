package service.api;

import com.sun.glass.ui.Menu;
import model.Note;
import repository.RepositoryApi;
import repository.repositoryevent.NoteCreateEvent;
import repository.repositoryevent.NoteDeleteEvent;
import service.Observer.observer.ObserverApi;
import service.Observer.observer.ObserverImpl;
import service.Observer.reciver.Receiver;

import java.beans.EventHandler;
import java.util.ArrayList;
import java.util.List;

public class LocalNoteApiImpl implements NoteAPI {
    private RepositoryApi repositoryApi;
    private ObserverApi<NoteCreateEvent> createObserver = new ObserverImpl<>(new ArrayList<Receiver<NoteCreateEvent>>());
    private ObserverApi<NoteDeleteEvent> deleteObserver = new ObserverImpl<>(new ArrayList<Receiver<NoteDeleteEvent>>());

    public LocalNoteApiImpl(RepositoryApi repositoryApi) {
        this.repositoryApi = repositoryApi;
    }

    @Override
    public Note create(Note note) {
        createObserver.execute(new NoteCreateEvent(this,repositoryApi.create(note)));
        return note;
    }

    @Override
    public Note get(long id) {
        return repositoryApi.getNote(id);
    }

    @Override
    public void delete(Note note) {
        repositoryApi.deleteNote( note.getId());
        deleteObserver.execute(new NoteDeleteEvent(this,note) );
    }

    @Override
    public List<Note> getAll() {
        return repositoryApi.getAll();
    }

    @Override
    public Note edit(Note note) {
        Note temp = repositoryApi.getNote( note.getId() );
       if ( temp.equals( note ) == true ) {
           return temp;
       }
       repositoryApi.deleteNote( temp.getId() );
       repositoryApi.create( note );
       return note;
    }

    @Override
    public void setOnCreateNoteReceiver(Receiver<NoteCreateEvent> noteCreateEventReceiver) {
        createObserver.addReceiver(noteCreateEventReceiver);
    }

    @Override
    public void removeCreateNoteReceiver(Receiver<NoteCreateEvent> noteCreateEventReceiver) {
        createObserver.removeReceiver(noteCreateEventReceiver);
    }

    @Override
    public void setOnDeleteNoteReceiver(Receiver<NoteDeleteEvent> noteDeleteEventReceiver) {
        deleteObserver.addReceiver(noteDeleteEventReceiver);
    }

    @Override
    public void removeDeleteNoteReceiver(Receiver<NoteDeleteEvent> noteDeleteEventReceiver) {
        deleteObserver.removeReceiver(noteDeleteEventReceiver);
    }

}

