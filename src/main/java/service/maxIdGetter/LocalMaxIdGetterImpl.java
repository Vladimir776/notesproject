package service.maxIdGetter;

import java.util.concurrent.atomic.AtomicLong;

public class LocalMaxIdGetterImpl implements MaxIdGetter{
    private AtomicLong maxId;
    public LocalMaxIdGetterImpl(long maxId) {
        this.maxId = new AtomicLong( maxId );
    }

    @Override
    public long getMaxId() {
        return maxId.get();
    }


    public void compareAndSet(Long o) {
        if ( o > maxId.get()) {
            maxId.compareAndSet(maxId.get(), o);
        }
    }
}
