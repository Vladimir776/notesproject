package service.maxIdGetter;

public interface MaxIdGetter {
    long getMaxId();
}
