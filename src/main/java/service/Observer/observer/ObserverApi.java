package service.Observer.observer;


import service.Observer.reciver.Receiver;

import java.util.EventObject;

public interface ObserverApi <E extends EventObject> {


    public void addReceiver(Receiver<E> receiver);


    public void removeReceiver(Receiver<E> receiver);

    public void execute(E event);


}
