package service.Observer.observer;

import service.Observer.reciver.Receiver;

import java.util.EventObject;
import java.util.List;

public class ObserverImpl<E extends EventObject> implements ObserverApi<E> {
    private final List<Receiver<E> >  receiverList;

    public ObserverImpl(List<Receiver<E>> receiverList) {
        this.receiverList = receiverList;
    }

    @Override
    public void addReceiver(Receiver<E> receiver) {
        receiverList.add(receiver);
    }

    @Override
    public void removeReceiver(Receiver<E> receiver) {
        receiverList.remove(receiver);
    }

    @Override
    public void execute(E event) {
        for (Receiver<E> iter:receiverList
             ) {
            iter.handler(event);
        }
    }
}
