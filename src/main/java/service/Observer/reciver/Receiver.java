package service.Observer.reciver;



import java.util.EventObject;

public interface Receiver<E extends EventObject> {

    void handler (E event);


}
