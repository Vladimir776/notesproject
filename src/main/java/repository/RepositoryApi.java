package repository;

import model.Note;
import repository.repositoryevent.NoteCreateEvent;
import repository.repositoryevent.NoteDeleteEvent;
import service.Observer.reciver.Receiver;

import java.util.List;

public interface RepositoryApi  {

    Note create(Note note);

    Note getNote(long id);

    void deleteNote(long id);

    List<Note> getAll();



}
