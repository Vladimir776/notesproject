package repository;

import exception.NoteNoFoundException;
import model.Note;
import model.noteevent.NoteIdChangedEvent;
import model.noteevent.NoteTextChangedEvent;
import repository.repositoryevent.NoteCreateEvent;
import repository.repositoryevent.NoteDeleteEvent;
import service.Observer.observer.ObserverApi;
import service.Observer.observer.ObserverImpl;
import service.Observer.reciver.Receiver;
import service.generator.IdGenerator;
import service.generator.LocalIdGeneratorImpl;
import service.maxIdGetter.LocalMaxIdGetterImpl;

import java.io.*;
import java.util.*;


public class LocalRepositoryImpl implements RepositoryApi {
    private final String saveLocation;
    private LocalMaxIdGetterImpl maxIdGetter = new LocalMaxIdGetterImpl(0L);
    private IdGenerator idGenerator ;
    private final Map<Long, Note> localBuffer = new HashMap<>();



    public LocalRepositoryImpl(String saveLocation) {
        this.saveLocation = saveLocation;
        File dir = new File(saveLocation);
        dir.mkdirs();


        List<File> fileList = Arrays.asList ( dir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".note");
            }
        }) );

        for (File file: fileList
             ) {
            try(ObjectInputStream oos  = new ObjectInputStream(new FileInputStream(file))) {
                Object obj = null;
                obj = oos.readObject();
                if (obj.getClass() == Note.class ) {
                    Note tempNote = (Note)obj;
                    localBuffer.put(tempNote.getId(),tempNote);
                    maxIdGetter.compareAndSet(tempNote.getId());
                }
            } catch (IOException e) {
                throw new NoteNoFoundException();
            } catch (ClassNotFoundException e) {
                throw new NoteNoFoundException();
            }
        }
        idGenerator = new LocalIdGeneratorImpl(maxIdGetter);

    }

    @Override
    public Note create(Note note) {
        Note temp =  localBuffer.get( note.getId() );
        if (temp != null) {
            return temp;
        } else if(note.getId() <= 0 ) {
            note.setId( idGenerator.generate() );
        }
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(generFile ( note ) ) ) ) {
                oos.writeObject(note);
        } catch (IOException e) {

        }
        localBuffer.put(note.getId() , note);
        return note;

    }

    @Override
    public Note getNote(long id) {
        Note temp =  localBuffer.get(id);
        if(temp == null) {
            throw new NoteNoFoundException();
        }
        return temp;
    }

    @Override
    public void deleteNote(long id) {
        Note temp =  localBuffer.get(id);
        if( temp != null ) {
            localBuffer.remove( id );
            generFile(temp).delete();
        } else {
            throw new NoteNoFoundException();
        }

    }

    @Override
    public List<Note> getAll() {
        List<Note> tempList = new ArrayList();
        tempList.addAll(localBuffer.values());
        return tempList;
    }




    private File generFile(Note note) {
       return new File((saveLocation + System.getProperty("file.separator") + note.getTitle()+note.getId() )
               .replaceAll("\\s+", "") + ".note");
    }




}
