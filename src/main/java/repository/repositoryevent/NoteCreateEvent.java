package repository.repositoryevent;

import model.Note;

import java.util.EventObject;

public class NoteCreateEvent extends EventObject {
    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    private final Note note;
    public NoteCreateEvent(Object source, Note note) {
        super(source);
        this.note = note;
    }
}
