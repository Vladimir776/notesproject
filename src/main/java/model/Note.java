package model;

import javafx.scene.control.Button;
import model.noteevent.NoteIdChangedEvent;
import model.noteevent.NoteTextChangedEvent;
import model.noteevent.NoteTitleChangedEvent;
import service.Observer.observer.ObserverApi;
import service.Observer.observer.ObserverImpl;
import service.Observer.reciver.Receiver;

import java.io.Serializable;
import java.util.ArrayList;

public class Note implements Serializable  {
    private static final long serialVersionUID = 752965680056100319L;
    protected long id;
    protected String title;
    protected String text;

    private ObserverApi<NoteIdChangedEvent> idObserver = new ObserverImpl<>(new ArrayList<Receiver<NoteIdChangedEvent>>());
    private ObserverApi<NoteTextChangedEvent> textObserver = new ObserverImpl<>(new ArrayList<Receiver<NoteTextChangedEvent>>());
    private ObserverApi<NoteTitleChangedEvent> titleObserver = new ObserverImpl<>(new ArrayList<Receiver<NoteTitleChangedEvent>>());

    public Note(long id) {
        this.id = id;
        title = "";
        text = "";
    }

    public Note() {
        id = 0;
        this.title = "";
        this.text = "";
        Button button = new Button();

    }

    public Note(long id , String title) {
        this.id = id;
        this.title = title;
        this.text = "";
    }

    public Note(long id , String title , String text) {
        this.id = id;
        this.title = title;
        this.text = text;
    }

    public Note(String title , String text) {
        this.id = 0;
        this.title = title;
        this.text = text;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        titleObserver.execute(new NoteTitleChangedEvent(this));

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        textObserver.execute(new NoteTextChangedEvent(this));
    }

    @Override
    public String toString() {
        return   title ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Note note = (Note) o;
        return id == note.id &&
                title.equals(note.title) &&
                text.equals(note.text);
    }



    public void setOnIdChange(Receiver<NoteIdChangedEvent> receiver) {
        idObserver.addReceiver(receiver);
    }
    public void removeIdReceiver(Receiver<NoteIdChangedEvent> receiver) {
        idObserver.removeReceiver(receiver);
    }

    public void setOnTitleChange(Receiver<NoteTitleChangedEvent> receiver) {
        titleObserver.addReceiver(receiver);
    }
    public void removeTitleReceiver(Receiver<NoteTitleChangedEvent> receiver) {
        titleObserver.removeReceiver(receiver);
    }

    public void setOnTextChange(Receiver<NoteTextChangedEvent> receiver) {
        textObserver.addReceiver(receiver);
    }
    public void removeTextReceiver(Receiver<NoteTextChangedEvent> receiver) {
        textObserver.removeReceiver(receiver);
    }
}
