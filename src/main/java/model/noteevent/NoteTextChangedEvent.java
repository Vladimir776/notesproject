package model.noteevent;

import model.Note;

import java.util.EventObject;

public class NoteTextChangedEvent extends EventObject {

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    private final String title;
    public NoteTextChangedEvent(Note source) {
        super(source);
        this.title = source.getTitle();
    }

    public String getTitle() {
        return title;
    }
}
