package model.noteevent;

import model.Note;

import java.util.EventObject;

public class NoteTitleChangedEvent extends EventObject {
    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    private final String text;
    public NoteTitleChangedEvent(Note source) {
        super(source);
        this.text = source.getText();
    }

    public String getText() {
        return text;
    }
}
