package model.noteevent;

import model.Note;

import java.util.EventObject;

public class NoteIdChangedEvent extends EventObject {

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    private final long Id;
    public NoteIdChangedEvent(Note source) {
        super(source);
        this.Id = source.getId();
    }

    public long getId() {
        return Id;
    }
}
